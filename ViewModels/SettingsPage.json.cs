using System;
using System.Collections.Generic;
using System.Linq;
using Starcounter;


using MailSync.Database;
using MailSync.Models;
using MailSync.Helpers;
using MailSync.Services;

using OAuth2.Core.Services;
using MailSync.Handlers;

namespace MailSync.ViewModels
{
    [SettingsPage_json.Test]
    partial class TestMail : Json
    {
        protected void Handle(Input.Send action)
        {
            OAUser user = SystemUser.Current.GetOAUser("Google");
            MailBox mb = Db.SQL<MailBox>("SELECT mb FROM MailSync.Database.MailBox mb WHERE mb.Owner=?", user).FirstOrDefault();

            Db.Transact(() =>
            {
                var mi = new MailItem()
                {
                    MailBox = mb,
                    Subject = action.App.Subject,
                    Body = action.App.Body
                };

                new MailContact()
                {
                    MailItem = mi,
                    MailContactType = MailContactTypeEnum.To,
                    Email = action.App.To
                };
            });
        }
    }

    partial class SettingsPage : Json
    {
        protected void Handle(Input.Remove action)
        {
            OAUser user = SystemUser.Current.GetOAUser("Google");

            MailBox mb = Db.SQL<MailBox>("SELECT mb FROM MailSync.Database.MailBox mb WHERE mb.Owner=?", user).FirstOrDefault();

            Db.Transact(() =>
            {
                var ms = MailServiceFactory.Get(mb);
                ms.RemoveWatch(mb);

                mb.Delete();
            });
        }
        
        public IEnumerable<MailItem> GetInbox(MailBox mb)
        {
            return Db.SQL<MailItem>("SELECT mi FROM MailSync.Database.MailItem mi WHERE mi.MailBox=? AND mi.Folder=? ORDER BY mi.Created DESC FETCH ?", mb, FolderEnum.Inbox, 5);
        }

        protected override void OnData()
        {
            base.OnData();

            OAUser user = SystemUser.Current.GetOAUser("Google");

            MailBox mb = Db.SQL<MailBox>("SELECT mb FROM MailSync.Database.MailBox mb WHERE mb.Owner=?", user).FirstOrDefault();
            
            // if user's mailbox isn't setup - do so
            if (null==mb)
            {
                Db.Transact(() =>
                {
                    mb = new MailBox()
                    {
                        Provider = "Google",
                        Name = user.Email,
                        Owner = user
                    };
                });
            }

            if(null==mb.WatchId)
            {
                var ms = MailServiceFactory.Get(mb);

                ms.AddWatch(mb);
            }

            this.MailBox = mb;
            this.MailboxName = mb.Name;
            this.Inbox.Data = GetInbox(mb);

            var session = Session.Ensure();
            session.Store["SettingsPage"] = this;

            MainHandlers.EnsureSavedSession(mb);
        }

        public MailBox MailBox { get; set; }
    }
}
