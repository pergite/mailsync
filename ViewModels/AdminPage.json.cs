using Starcounter;

using OAuth2.Core.Models;
using MailSync.Helpers;

namespace MailSync.ViewModels
{
    partial class AdminPage : Json
    {
        protected override void OnData()
        {
            base.OnData();
        }

        protected void Handle(Input.Save action)
        {
            if (this.Transaction.IsDirty)
            {
                this.Transaction.Commit();
            }

            this.SuccessMessage = "Changes saved";
        }

        protected void Handle(Input.Undo action)
        {
            if (this.Transaction.IsDirty)
            {
                this.Transaction.Rollback();
            }

            this.SuccessMessage = "Changes reverted";
        }
    }
}
