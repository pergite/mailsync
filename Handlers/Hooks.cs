﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Starcounter;
using MailSync.Database;
using MailSync.Services;
using MailSync.Helpers;

namespace MailSync.Handlers
{
    public class Hooks
    {
        public static void Register()
        {
            Hook<MailItem>.CommitInsert += (s, mi) => {

                if (null == mi.MailBox)
                {
                    throw new InvalidOperationException("Must be assigned to a MailBox");
                }

                if (mi.Folder != FolderEnum.Outbox)
                {
                    // Inbox & Sent is readonly for outsiders!
                    throw new InvalidOperationException("Can only create MailItem in Outbox folder");
                }

                var mb = mi.MailBox;

                mi.From = new MailContact()
                {
                    Email = mb.Owner.Email,
                    Name = mb.Owner.Name
                };

                MailServiceFactory.Get(mb).Send(mi);

                mi.Created = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc);
                mi.Folder = FolderEnum.Sent;
            };
        }
    }
}
