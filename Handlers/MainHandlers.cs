﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Starcounter;

using MailSync.Database;
using MailSync.Helpers;
using MailSync.ViewModels;

namespace MailSync.Handlers
{
    public class MainHandlers
    {
        public static void Register()
        {
            Handle.GET("/mailsync/admin", () =>
            {
                if (!SystemUser.Current.AllowAdminSettings)
                {
                    return Self.GET("/mailync/unauthorized?return_uri=" + HttpUtility.UrlEncode("/mailsync/admin"));
                }

                return Db.Scope(() => {
                    return new AdminPage()
                    {
                        Data = SettingsManager.Instance.Data
                    };
                });
            });

            Handle.GET("/mailsync/settings", () =>
            {
                if (!SettingsManager.Instance.Data.GoogleEnabled)
                {
                    return Self.GET("/mailync/admin");
                }

                var user = SystemUser.Current;

                if (!user.AllowSyncMailbox)
                {
                    return Self.GET("/mailync/unauthorized?return_uri=" + HttpUtility.UrlEncode("/mailsync/settings"));
                }

                // check if current user has a linked google login
                if (null != user.GetOAUser("Google"))
                {
                    var page = new SettingsPage()
                    {
                        Data = null
                    };

                    return Db.Scope(() => { return page; });
                }

                return new SigninPage()
                {
                    Scope = HttpUtility.UrlEncode("https://www.googleapis.com/auth/gmail.readonly https://www.googleapis.com/auth/gmail.send"),
                    FinalDestination = HttpUtility.UrlEncode("/mailsync/settings")
                };
            });

            // Blending
            Blender.MapUri("/mailsync/unauthorized?return_uri={?}", "userform-return", null);
        }

        public static void EnsureSavedSession(MailBox mb)
        {
            var savedSession = Db.SQL<SavedSession>("SELECT s FROM SavedSession s WHERE s.SessionId = ?", Session.Current.SessionId).FirstOrDefault();

            if (savedSession == null)
            {
                Session.Current.AddDestroyDelegate(s =>
                {
                    Db.Transact(() =>
                    {
                        var saved = Db.SQL<SavedSession>("SELECT s FROM SavedSession s WHERE s.SessionId = ?", s.SessionId).FirstOrDefault();
                        if (null != saved)
                            saved.Delete();
                    });
                });

                Db.Transact(() =>
                {
                    new SavedSession
                    {
                        SessionId = Session.Current.SessionId,
                        Provider = mb.Provider,
                        Name =mb.Name
                    };
                });
            }
        }
    }
}
