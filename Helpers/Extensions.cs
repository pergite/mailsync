﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Net.Mail;
using System.Reflection;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Threading;

using Starcounter;
using MailSync.Database;
using MailSync.Models;
using MailSync.Services;


namespace MailSync.Helpers
{
    public static class Extensions
    {
        static DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static string ISO8601(this DateTime dt)
        {
            return dt.ToString("yyyy-MM-ddTHH:mm:ss.msZ");
        }

        public static DateTime FromUnixTime(this long unixTime)
        {
            return epoch.AddMilliseconds(unixTime);
        }

        public static long ToUnixTime(this DateTime date)
        {
            return Convert.ToInt64((date - epoch).TotalMilliseconds);
        }

        public static string UrlEncode(this string s)
        {
            return HttpUtility.UrlEncode(s);
        }

        public static int IndexOf(this StringBuilder sb, char c)
        {
            for (int i = 0; i < sb.Length; i++)
            {
                if (sb[i] == c)
                    return i;
            }

            return -1;
        }

        public static Task WaitOneAsync(this WaitHandle waitHandle)
        {
            if (waitHandle == null)
                throw new ArgumentNullException("waitHandle");

            var tcs = new TaskCompletionSource<bool>();
            var rwh = ThreadPool.RegisterWaitForSingleObject(waitHandle,
                delegate { tcs.TrySetResult(true); }, null, -1, true);
            var t = tcs.Task;
            t.ContinueWith((antecedent) => rwh.Unregister(null));
            return t;
        }

        public static OAUser GetOAUser(this SystemUser user, string providerName)
        {
            return Db.SQL<OAUser>("SELECT u FROM MailSync.Database.OAUser u WHERE u.SystemUser=? AND u.Provider=?", user, providerName).FirstOrDefault();
        }

        public static OAuth2.Core.Services.ServiceBase GetService(this OAUser user)
        {
            var settings = SettingsManager.Instance.GetSettings(user.Provider);
            var tm = TokenManager.Get(user);

            return OAuth2.Core.Services.Factory.Get(settings, tm);
        }

        public static void WriteToStream(this MailMessage message, Stream s)
        {
            // Ok, this is "hacky", but as the MailWriter class is internal
            // we have no option but to resort to reflection in order to
            // serialize the MailMessage onto a stream.
            // Of course there are other 3rd party libs to do this, but as the
            // functionality *is* builtin .Net....

            BindingFlags flags = BindingFlags.Instance | BindingFlags.NonPublic;
            Assembly assembly = typeof(SmtpClient).Assembly;

            // var mailWriter = new MailWriter(s);
            Type mailWriterType = assembly.GetType("System.Net.Mail.MailWriter");
            ConstructorInfo mailWriterConstructor = mailWriterType.GetConstructor(flags, null, new[] { typeof(Stream) }, null);
            object mailWriter = mailWriterConstructor.Invoke(new object[] { s });

            // message.Send(mailWriter);
            MethodInfo sendMethod = typeof(MailMessage).GetMethod("Send", flags);
            sendMethod.Invoke(message, flags, null, new[] { mailWriter, true, true }, null);

            // mailWriter.Close();
            MethodInfo closeMethod = mailWriter.GetType().GetMethod("Close", flags);
            closeMethod.Invoke(mailWriter, flags, null, new object[] { }, null);
        }

        public static string GetAsString(this MailMessage message)
        {
            using (var ms = new MemoryStream())
            {
                WriteToStream(message, ms);
                ms.Seek(0, SeekOrigin.Begin);

                using (var sr = new StreamReader(ms))
                {
                    return sr.ReadToEnd();
                }
            }
        }

        public static string Base64UrlEncode(this string text)
        {
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(text);

            return System.Convert.ToBase64String(bytes)
                .Replace('+', '-')
                .Replace('/', '_')
                .Replace("=", "");
        }

        public static byte[] Base64UrlDecode(this string text)
        {
            // "DQoNCj4gT24gMjQgTm92IDIwMTcsIGF0IDE2OjQ4LCBqb2hhbm85OUBnbWFpbC5jb20gd3JvdGU6DQo-IA0KPiB0ZXN0bWVzc2FnZQ0KDQo="
            string s = text;
            s = s.Replace('-', '+'); // 62nd char of encoding
            s = s.Replace('_', '/'); // 63rd char of encoding
            switch (s.Length % 4) // Pad with trailing '='s
            {
                case 0: break; // No pad chars in this case
                case 2: s += "=="; break; // Two pad chars
                case 3: s += "="; break; // One pad char
                default:
                    throw new System.Exception(
             "Illegal base64url string!");
            }
            return Convert.FromBase64String(s);
        }

        public static string GetHeader(this ICollection<GHeader> headers, string name, bool ignoreCase=true, string defaultValue="")
        {
            if (null != headers)
            {
                foreach (var h in headers)
                {
                    if (0 == String.Compare(h.name, name, ignoreCase))
                    {
                        return h.value;
                    }
                }
            }

            return defaultValue;
        }

        public static MailContact CreateMailContact(this MailAddress mailAddress, MailItem mailItem, MailContactTypeEnum contactType)
        {
            return new MailContact()
            {
                MailItem = mailItem,
                Email = mailAddress.Address,
                Name = mailAddress.DisplayName,
                MailContactType = contactType
            };
        }
    }
}
