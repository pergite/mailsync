﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Starcounter;
using OAuth2.Core.Models;

using MailSync.Database;
using MailSync.Models;

namespace MailSync.Helpers
{
    public class SettingsManager : ISettingsManager
    {
        protected SettingsManager()
        {
        }

        public Settings Data
        {
            get
            {
                return Db.SQL<Settings>("SELECT s FROM MailSync.Database.Settings s").FirstOrDefault();
            }
        }

        public ISettings GetSettings(string serviceName)
        {
            var settings = Data;

            switch(serviceName.ToLower())
            {
                case "google":

                    if(settings.GoogleEnabled)
                    {
                        return new OASettings()
                        {
                            DisplayName = "Google",
                            ServiceName = "google",
                            ClientID = settings.GoogleClientID,
                            ClientSecret = settings.GoogleClientSecret
                        };
                    }
                    break;

                case "microsoft":

                    if (settings.MicrosoftEnabled)
                    {
                        return new OASettings()
                        {
                            DisplayName = "Microsoft",
                            ServiceName = "microsoft",
                            ClientID = settings.MicrosoftClientID,
                            ClientSecret = settings.MicrosoftClientSecret
                        };
                    }
                    break;
            }

            return null;
        }

        public static SettingsManager Instance
        {
            get
            {
                if (null == Db.SQL<Settings>("SELECT s FROM MailSync.Database.Settings s").FirstOrDefault())
                {
                    Db.Transact(() =>
                    {

                        new Settings()
                        {
                            GoogleEnabled = false,
                            MicrosoftEnabled = false
                        };
                    });
                }

                return new SettingsManager();
            }
        }
    }
}
