﻿using System;
using System.Linq;
using System.Collections.Generic;

using Starcounter;

using MailSync.Database;

namespace MailSync.Helpers
{
    public static class MBHelper
    {
        public static MailBox Get(string provider, string watchId)
        {
            return Db.SQL<MailBox>("SELECT mb FROM MailSync.Database.MailBox mb WHERE mb.Provider=? AND mb.WatchRemoteId=?", provider, watchId).FirstOrDefault();
        }
    }
}
