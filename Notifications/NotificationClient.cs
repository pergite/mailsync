﻿using System;
using System.Threading;
using System.Net.Sockets;

using Starcounter;

using MailSync.Database;
using MailSync.Models;
using MailSync.Helpers;
using MailSync.Services;

namespace MailSync.Notifications
{
    public class NotificationClient
    {
        protected const string SERVER_ENDPOINT = "eventdispatchor.pergite.com";
        //protected const string SERVER_ENDPOINT = "eventdispatchor.pergite.local.com";
        protected const int SERVER_PORT = 9999;

        protected static NotificationClient client = null;
        protected static object _lock = new object();

        protected bool askedToStop = false;
        protected Thread thread;
        protected ManualResetEvent reset;

        public static void Start()
        {
            if (null == client)
            {
                lock (_lock)
                {
                    if (null == client)
                        client = new NotificationClient();
                }
            }
        }

        // Havent found any event to hook up to in order to detect if our Starcounter app is going down 
        // so the Stop() would currently never be called. Its still left inplace in case such an
        // event suddenly emerges
        public static void Stop()
        {
            if (null == client)
                throw new InvalidOperationException("NotificationClient not running");

            client.askedToStop = true;
            client.reset.Set();
        }

        public static void Reset()
        {
            if (null == client)
                throw new InvalidOperationException("NotificationClient not running");

            client.reset.Set();
        }

        public static void AddWatch(MailBox mb)
        {
            var ms = MailServiceFactory.Get(mb);
            ms.AddWatch(mb);

            // reregister
            client.reset.Set();
        }

        public static void RemoveWatch(MailBox mb)
        {
            if (String.IsNullOrEmpty(mb.WatchId))
                return;

            var ms = MailServiceFactory.Get(mb);
            ms.RemoveWatch(mb);

            // reregister
            client.reset.Set();
        }

        public static void RefreshWatch(MailBox mb)
        {
            if (String.IsNullOrEmpty(mb.WatchId))
                return;

            var ms = MailServiceFactory.Get(mb);
            ms.RefreshWatch(mb);

            // reregister
            client.reset.Set();
        }

        public NotificationClient()
        {
            reset = new ManualResetEvent(false);
            thread = new Thread(this.Run);
            thread.Start();
        }

        public void Run()
        {
            Console.WriteLine("NotificationhClient: started!");

            while (!askedToStop)
            {
                Console.WriteLine($"NotificationhClient:  Connecting to {SERVER_ENDPOINT}:{SERVER_PORT}");

                try
                {
                    using (TcpClient client = new TcpClient(SERVER_ENDPOINT, SERVER_PORT))
                    {
                        using (JsonSocketReaderWriter s = new JsonSocketReaderWriter(client.GetStream(), 128)) // readbuffersize=128, enough for one WCNotification
                        {
                            while (true)
                            {
                                // register us!
                                NotificationRegistration register = new NotificationRegistration();

                                Scheduling.RunTask(() =>
                                {
                                    foreach (var mb in Db.SQL<MailBox>("SELECT mb FROM MailSync.Database.MailBox mb WHERE mb.WatchId IS NOT NULL"))
                                    {
                                        register.Channels.Add(mb.WatchRemoteId);
                                    }

                                }).Wait();

                                Console.WriteLine(String.Format("NotificationClient: Registering channels [{0}]", String.Join(",", register.Channels)));

                                s.SendJson(register);

                                reset.Reset();

                                // enter WCNotification loop
                                while (true)
                                {
                                    Notification wcn = s.ReadJson<Notification>(reset);

                                    if (null == wcn)
                                    {
                                        if (reset.WaitOne(0))
                                        {
                                            // reregister (or quit if askedToStop)
                                            break;
                                        }

                                        // server disconnected us
                                        throw new ServerDisconnectedException();
                                    }
                                    else
                                    {
                                        Console.WriteLine($"NotificationhClient: Notification => {wcn.Provider}:{wcn.ChannelId}:{wcn.Message}");

                                        if (wcn.Message == "sync")
                                        {
                                            // TODO?
                                        }
                                        else
                                        {
                                            Scheduling.RunTask(() =>
                                            {
                                                MailBox mb = MBHelper.Get(wcn.Provider, wcn.ChannelId);

                                                if (null != mb)
                                                {
                                                    MailService.QueueIncrSync(mb);
                                                }
                                                else
                                                {
                                                    Console.WriteLine($"NotificationhClient: No MailBox with Provider:WatchId {wcn.Provider}:{wcn.ChannelId}");
                                                }

                                            });
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (ServerDisconnectedException)
                {
                    Console.WriteLine("NotificationhClient: Server disconnected us! Reconnecting in 2s");
                    Thread.Sleep(2000);
                }
                catch (SocketException)
                {
                    Console.WriteLine($"NotificationhClient: Failed/lost connection to {SERVER_ENDPOINT}! Retrying in 2s");
                    Thread.Sleep(2000);
                }
            }

            Console.WriteLine("NotificationClient: asked to stop, terminating gracefully");
        }
    }
}
