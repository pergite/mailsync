﻿using System;
using Starcounter;

namespace MailSync.Database
{
    [Database]
    public class Settings
    {
        public bool GoogleEnabled { get; set; }
        public string GoogleClientID { get; set; }
        public string GoogleClientSecret { get; set; }
        public string GooglePushTopicName { get; set; }

        public bool MicrosoftEnabled { get; set; }
        public string MicrosoftClientID { get; set; }
        public string MicrosoftClientSecret { get; set; }
    }
}
