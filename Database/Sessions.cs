﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starcounter;

namespace MailSync.Database
{
    [Database]
    public class SavedSession
    {
        public string SessionId;
        public string Provider;
        public string Name;
    }
}
