﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starcounter;

namespace MailSync.Database
{
    public enum FolderEnum { Inbox=1, Sent=2, Outbox=3 }
    public enum MailContactTypeEnum { From = 0, To = 1, CC = 2, BCC = 3 }

    [Database]
    public class MailBox
    {
        public string Provider { get; set; }
        public OAUser Owner { get; set; }
        
        public string Name { get; set; }

        public IEnumerable<MailItem> Inbox => Db.SQL<MailItem>("SELECT mi FROM MailSync.Database.MailItem mi WHERE mi.MailBox=? AND mi.Folder=?", this, FolderEnum.Inbox);
        public IEnumerable<MailItem> Outbox => Db.SQL<MailItem>("SELECT mi FROM MailSync.Database.MailItem mi WHERE mi.MailBox=? AND mi.Folder=?", this, FolderEnum.Outbox);
        public IEnumerable<MailItem> Sent => Db.SQL<MailItem>("SELECT mi FROM MailSync.Database.MailItem mi WHERE mi.MailBox=? AND mi.Folder=?", this, FolderEnum.Sent);

        public DateTime Updated { get; set; }

        /* INTERNALS - DO NOT MAP */
        public string SyncToken { get; set; }
        public string WatchId { get; set; }
        public string WatchRemoteId { get; set; }
        public DateTime WatchExpires { get; set; }
        /* EMD INTERNALS */
    }

    [Database]
    public class MailContact
    {
        public MailItem MailItem { get; set; }
        public MailContactTypeEnum MailContactType { get; set; }

        public string Email { get; set; }
        public string Name { get; set; }
    }

    [Database]
    public class MailItem
    {
        public MailBox MailBox { get; set; }

        public FolderEnum Folder { get; set; }

        public MailContact From { get; internal set; }

        public IEnumerable<MailContact> To => Db.SQL<MailContact>("SELECT mc FROM MailSync.Database.MailContact mc WHERE mc.MailItem=? AND mc.MailContactType=?", this, MailContactTypeEnum.To);
        public IEnumerable<MailContact> CC => Db.SQL<MailContact>("SELECT mc FROM MailSync.Database.MailContact mc WHERE mc.MailItem=? AND mc.MailContactType=?", this, MailContactTypeEnum.CC);
        public IEnumerable<MailContact> BCC =>  Db.SQL<MailContact>("SELECT mc FROM MailSync.Database.MailContact mc WHERE mc.MailItem=? AND mc.MailContactType=?", this, MailContactTypeEnum.BCC);

        public string Subject { get; set; }
        public string Body { get; set; }

        public DateTime Created { get; internal set; }

        public MailItem()
        {
            Folder = FolderEnum.Outbox;
        }

        /* INTERNALS - DO NOT MAP */
        public string RemoteId { get; set; }
        /* END INTERNALS */
    }
}
