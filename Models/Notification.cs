﻿using System;
using System.Collections.Generic;

namespace MailSync.Models
{
    public class NotificationRegistration
    {
        public string Provider;
        public List<string> Channels;

        public NotificationRegistration()
        {
            Channels = new List<string>();
        }

    }

    public class Notification
    {
        public string Provider;
        public string ChannelId;
        public string Message;
        public string SyncToken;
    }
}
