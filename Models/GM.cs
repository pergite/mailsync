﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailSync.Models
{
    public class GHeader
    {
        public string name;
        public string value;
    }

    /*
        {
          "attachmentId": string,
          "size": integer,
          "data": bytes
        }     
    */
    public class GResource
    {
        public string attachmentId;
        public int size;
        public string data;
    }

    public class GPayload
    {
        public string partId;
        public string mimeType;
        public string fileName;
        public GHeader[] headers;
        public GResource body;
        public GPayload[] parts;
    }

    /*
        {
          "id": string,
          "threadId": string,
          "labelIds": [
            string
          ],
          "snippet": string,
          "historyId": unsigned long,
          "internalDate": long,
          "payload": {
            "partId": string,
            "mimeType": string,
            "filename": string,
            "headers": [
              {
                "name": string,
                "value": string
              }
            ],
            "body": users.messages.attachments Resource,
            "parts": [
              (MessagePart)
            ]
          },
          "sizeEstimate": integer,
          "raw": bytes
        }
    */
    public class GMessage
    {
        public string id;
        public string threadId;
        public string[] labelIds;
        public string snippet;
        public ulong historyId;
        public long internalDate;

        public GPayload payload;

        public int sizeEstimate;
        public string raw;
    }


    /*
        {
          "messages": [
            users.messages Resource
          ],
          "nextPageToken": string,
          "resultSizeEstimate": unsigned integer
        }     
    */

    public class GMessages
    {
        public GMessage[] messages;
        public string nextPageToken;
        public uint resultSizeEstimate;
    }

    /*
        {
          "history": [
            {
              "id": unsigned long,
              "messages": [
                users.messages Resource
              ],
              "messagesAdded": [
                {
                  "message": users.messages Resource
                }
              ],
              "messagesDeleted": [
                {
                  "message": users.messages Resource
                }
              ],
              "labelsAdded": [
                {
                  "message": users.messages Resource,
                  "labelIds": [
                    string
                  ]
                }
              ],
              "labelsRemoved": [
                {
                  "message": users.messages Resource,
                  "labelIds": [
                    string
                  ]
                }
              ]
            }
          ],
          "nextPageToken": string,
          "historyId": unsigned long
        }    
    */
    public class GHistoryMessage
    {
        public GMessage message;
    }

    public class GHistoryLabel
    {
        public GMessage message;
        public string[] labelIds;
    }

    public class GHistory
    {
        public long id;
        public GMessage[] messages;
        public GHistoryMessage[] messagesAdded;
        public GHistoryMessage[] messagesDeleted;
        public GHistoryLabel[] labelsAdded;
        public GHistoryLabel[] labelsRemoved;
    }

    public class GHistoryResponse
    {
        public GHistory[] history;
        public string nextPageToken;
        public ulong historyId;
    }

    /*
        POST "https://www.googleapis.com/gmail/v1/users/me/watch"
        Content-type: application/json

        {
          topicName: "projects/myproject/topics/mytopic",
          labelIds: ["INBOX"],
        }
    */
    public class GMWatchRequest
    {
        public string topicName;
        public string[] labelIds;
    }


    /*
        {
          historyId: 1234567890
          expiration: 1431990098200
        }     
    */
    public class GMWatchResponse
    {
        public long historyId;
        public long expiration;
    }
}
