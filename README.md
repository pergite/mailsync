# README #

The MailSync app is intended to provide a GMail inbox/outbox to other Starcounter apps, giving them the 
possibility to send and receive emails using a GMail account. The app requires Mapping to be inplace
and map the tables Database\SystemUser, Database\MailBox and Database\MailItem classes.

Emails are sent when a new object of class MailItem is created.

The app has a "settings" page which sets up the MailBox sync for the currently logged on user.
(If no mapping is in place, a "fake"-admin user would be JIT-created)

The settings page also shows the 5 latest received emails as well as a simple mail test form.

### How do I get set up? ###

* Requires Starcounter 2.4

* Requires Mapping to be in place in order to be used from other apps.

* First, You need to provide OAuth2 ClientID/ClientSecrets as well as a Google CloudSync topic.
  (for "in-house" testpurposes, you can email johan.olofsson@pergite.com and ask for my app registration
  which assumes http://127.0.0.1:8080/  callback urls)
  
  Setup is done at the url: http://127.0.0.1:8080/MailSync/Admin
  
* MailBox sync is then created by visiting the url: http://127.0.0.1:8080/MailSync/Settings


### Who do I talk to? ###

* johan.olofsson@pergite.com
