﻿using System;
using System.Linq;
using Starcounter;

using MailSync.Handlers;
using MailSync.Helpers;
using MailSync.Database;
using MailSync.Notifications;

using OAuth2.Core.Handlers;

namespace Mailbox
{
    class Program
    {
        static void Main()
        {
            Application.Current.Use(new HtmlFromJsonProvider());
            Application.Current.Use(new PartialToStandaloneHtmlProvider());

            Db.Transact(() =>
            {
                Db.SlowSQL("DELETE FROM MailSync.Database.SavedSession");
            });

            /*
            var oa = Db.SQL<OAUser>("SELECT oa FROM OAuser oa").First;
            var ms = MailSync.Services.MailServiceFactory.Get("google", oa);
            ms.RemoveWatch(null);
            */

            OAuth2Flow.Register(new Uri("/mailsync/", UriKind.Relative), SettingsManager.Instance, UserManager.Instance);

            MainHandlers.Register();

            Hooks.Register();

            NotificationClient.Start();

            /*
            var mb = Db.SQL<MailBox>("SELECT mb FROM MailBox mb").FirstOrDefault();
            var ms = MailSync.Services.MailServiceFactory.Get(mb);
            ms.IncrSync(mb);
            */
        }

    }
}
