﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text.RegularExpressions;

using Starcounter;

using OAuth2.Core.Services;
using OAuth2.Core.Helpers;

using MailSync.Database;
using MailSync.Models;
using MailSync.Helpers;

namespace MailSync.Services
{
    public class GMail : MailService
    {
        protected static Regex invalidMailAddressCharacters = new Regex(@"[""'\\]", RegexOptions.Compiled);

        public override void Send(MailItem mi)
        {
            var m = new MailMessage()
            {
                From = new MailAddress(mi.MailBox.Owner.Email),
                Subject = mi.Subject,
                Body = mi.Body
            };

            foreach (MailContact mc in mi.To)
                m.To.Add(mc.Email);

            foreach (MailContact mc in mi.CC)
                m.CC.Add(mc.Email);

            foreach (MailContact mc in mi.BCC)
                m.Bcc.Add(mc.Email);

            var whatAMess = this.Rest.Post<GMessage>("https://www.googleapis.com/upload/gmail/v1/users/me/messages/send?uploadType=media", (s) => { m.WriteToStream(s); }, "message/rfc822", "*/*");
        }

        public override void IncrSync(MailBox mb)
        {
            var parameters = new Dictionary<string, RequestParameterValue>
            {
                { "historyTypes", new [] { "messageAdded", "messageDeleted" } },
                { "labelId", "INBOX" },
                { "maxResults", "250" },
                { "pageToken", null },
                { "startHistoryId", mb.SyncToken }
            };

            TimeZoneInfo tziUtc = TimeZoneInfo.Utc;
            string nextSyncToken = null;

            DbX.TransactIgnoreHooks(() =>
            {
                do
                {
                    var result = this.Rest.Get<GHistoryResponse>($"https://www.googleapis.com/gmail/v1/users/me/history?{RequestParameters.Build(parameters)}");

                    if(null!=result.history) {

                        foreach (var h in result.history)
                        {
                            if(null!=h.messagesAdded)
                            {
                                foreach(var m in h.messagesAdded)
                                {
                                    var gmailItem = this.Rest.Get<GMessage>($"https://www.googleapis.com/gmail/v1/users/me/messages/{m.message.id}?format=full");

                                    var mailItem = Db.SQL<MailItem>("SELECT mi FROM MailSync.Database.MailItem mi WHERE mi.MailBox=? AND mi.RemoteId=?", mb, gmailItem.id).FirstOrDefault();

                                    if(gmailItem.labelIds.Contains("INBOX"))
                                    {
                                        if (null == mailItem)
                                        {
                                            string body = null;
                                            string subject = gmailItem.payload.headers.GetHeader("Subject", true);

                                            var from = new MailAddress(gmailItem.payload.headers.GetHeader("From"));

                                            var to = new List<MailAddress>();
                                            var cc = new List<MailAddress>();

                                            foreach (var s in gmailItem.payload.headers.GetHeader("To").Split(','))
                                            {
                                                if (!String.IsNullOrEmpty(s))
                                                    to.Add(new MailAddress(invalidMailAddressCharacters.Replace(s,"")));
                                            }

                                            foreach (var s in gmailItem.payload.headers.GetHeader("CC").Split(','))
                                            {
                                                if(!String.IsNullOrEmpty(s))
                                                    cc.Add(new MailAddress(invalidMailAddressCharacters.Replace(s, "")));
                                            }


                                            if (null != gmailItem.payload.body.data)
                                            {
                                                body = System.Text.Encoding.UTF8.GetString(gmailItem.payload.body.data.Base64UrlDecode());
                                            }
                                            else
                                            {
                                                // ok, walk parts..
                                                foreach(var part in gmailItem.payload.parts)
                                                {
                                                    if(part.mimeType == "text/plain")
                                                    {
                                                        body = System.Text.Encoding.UTF8.GetString(part.body.data.Base64UrlDecode());
                                                        break;
                                                    }
                                                }
                                            }

                                            mailItem = new MailItem()
                                            {
                                                RemoteId = gmailItem.id,
                                                MailBox = mb,
                                                Folder = FolderEnum.Inbox,
                                                Subject = subject,
                                                Body = body,
                                                Created = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc)
                                            };

                                            mailItem.From = from.CreateMailContact(mailItem, MailContactTypeEnum.From);

                                            foreach(MailAddress ma in to)
                                            {
                                                ma.CreateMailContact(mailItem, MailContactTypeEnum.To);
                                            }

                                            foreach (MailAddress ma in cc)
                                            {
                                                ma.CreateMailContact(mailItem, MailContactTypeEnum.CC);
                                            }

                                            Console.WriteLine("Received mail from {0}", mailItem.From.Email);
                                        }
                                    }
                                    else if(null!=mailItem)
                                    {
                                        mailItem.Delete();
                                    }
                                }
                            }

                            if (null != h.messagesDeleted)
                            {
                                foreach (var m in h.messagesDeleted)
                                {
                                    var mailItem = Db.SQL<MailItem>("SELECT mi FROM MailSync.Database.MailItem mi WHERE mi.MailBox=? AND mi.RemoteId=?", mb, m.message.id).FirstOrDefault();

                                    if(null!=mailItem)
                                    {
                                        mailItem.Delete();
                                    }
                                }
                            }
                        }
                    }

                    parameters["pageToken"] = result.nextPageToken;
                    nextSyncToken = result.historyId.ToString();

                } while (null != parameters["pageToken"].Value);

                mb.SyncToken = nextSyncToken;
                mb.Updated = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc);
            });
        }

        public override void FullSync(MailBox mb)
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>()
            {
                { "includeSpamTrash", "false" },
                { "labelIds", "INBOX" },
                { "maxResults", "250" },
                { "pageToken", null }
            };

            TimeZoneInfo tziUtc = TimeZoneInfo.Utc;

            DbX.TransactIgnoreHooks(() =>
            {
                do
                {
                    var result = this.Rest.Get<GMessages>($"https://www.googleapis.com/gmail/v1/users/me/messages?{RequestParameters.Build(parameters)}");

                    foreach (var m in result.messages)
                    {
                    }

                    parameters["pageToken"] = result.nextPageToken;

                } while (null != parameters["pageToken"]);
            });
        }

        public override void AddWatch(MailBox mb)
        {
            Settings s = SettingsManager.Instance.Data;

            if (String.IsNullOrEmpty(s.GooglePushTopicName))
                throw new InvalidOperationException("Google Push TopicName must not be null");

            var req = new GMWatchRequest()
            {
                topicName = s.GooglePushTopicName,
                labelIds = new string[] { "INBOX" }
            };

            var resp = this.Rest.Post<GMWatchRequest, GMWatchResponse>("https://www.googleapis.com/gmail/v1/users/me/watch", req);

            Db.Transact(() =>
            {
                mb.SyncToken = resp.historyId.ToString();

                mb.WatchId = Guid.NewGuid().ToString("D");
                mb.WatchRemoteId = mb.Name;
                mb.WatchExpires = resp.expiration.FromUnixTime();
            });
        }

        public override void RemoveWatch(MailBox mb)
        {
            this.Rest.Post("https://www.googleapis.com/gmail/v1/users/me/stop", NoContent.Data);

            Db.Transact(() =>
            {
                mb.WatchId = null;
                mb.WatchRemoteId = null;
                mb.WatchExpires = DateTime.MinValue;
            });
        }

        public override void RefreshWatch(MailBox mb)
        {
            Db.Transact(() => {

                this.Rest.Post<NoContent>("https://www.googleapis.com/gmail/v1/users/me/stop", NoContent.Data);

                var req = new GMWatchRequest()
                {
                    topicName = SettingsManager.Instance.Data.GooglePushTopicName,
                    labelIds = new string[] { "INBOX" }
                };

                var resp = this.Rest.Post<GMWatchRequest, GMWatchResponse>("https://www.googleapis.com/gmail/v1/users/me/watch", req);

                Db.Transact(() =>
                {
                    mb.WatchId = Guid.NewGuid().ToString("D");
                    mb.WatchRemoteId = "CloudSync doesnt use an Id";
                    mb.WatchExpires = resp.expiration.FromUnixTime();
                });
            });
        }
    }
}
