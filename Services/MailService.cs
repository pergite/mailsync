﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Starcounter;
using MailSync.Database;
using MailSync.ViewModels;

using OAuth2.Core.Services;

namespace MailSync.Services
{
    public abstract class MailService
    {
        public SystemUser User { get; set; }
        public ServiceBase Rest { get; set; }

        public static ConcurrentDictionary<MailBox, int> incrSyncQueue = new ConcurrentDictionary<MailBox, int>();

        public static void QueueIncrSync(MailBox mb)
        {
            var count = incrSyncQueue.AddOrUpdate(mb, 1, (k, i) => i+1);
            if (count > 1)
                return; //already scheduled

            Scheduling.RunTask(() =>
            {
                while (true)
                {
                    MailServiceFactory.Get(mb).IncrSync(mb);

                    var nc = incrSyncQueue.AddOrUpdate(mb, 1, (k, i) => i-1);
                    if (nc <= 0)
                    {
                        // calculate patches to live sessions
                        var sessions = Db.SQL<SavedSession>("SELECT s FROM MailSync.Database.SavedSession s WHERE s.Provider=? AND s.Name=?", mb.Provider, mb.Name).Select(ss => ss.SessionId).ToList();

                        Session.RunTask(sessions, (Session s, string sessionId) =>
                        {
                            var settingsPage = s.Store["SettingsPage"] as SettingsPage;

                            if (null != settingsPage)
                            {
                                settingsPage.Inbox.Data = settingsPage.GetInbox(mb);
                                s.CalculatePatchAndPushOnWebSocket();
                            }
                        });

                        break;
                    }
                }
            });
        }

        public abstract void Send(MailItem mi);
        public abstract void IncrSync(MailBox mb);
        public abstract void FullSync(MailBox mb);
        
        public abstract void AddWatch(MailBox mailBox);
        public abstract void RemoveWatch(MailBox mailBox);
        public abstract void RefreshWatch(MailBox mailBox);
    }
}
