﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MailSync.Database;
using MailSync.Helpers;

namespace MailSync.Services
{
    public class MailServiceFactory
    {
        public static MailService Get(string name, OAUser user)
        {
            if (name.ToLower() == "google")
            {
                var sm = SettingsManager.Instance;
                var tm = TokenManager.Get(user);

                return new GMail()
                {
                    Rest = OAuth2.Core.Services.Factory.Get(sm.GetSettings("google"), tm),
                    User = user.SystemUser
                };
            }

            throw new NotImplementedException(String.Format("MailSync service '{0}' not implemented", name));
        }

        public static MailService Get(MailBox mb)
        {
            return Get(mb.Provider, mb.Owner);
        }
    }
}
